﻿using FluentValidation;
using FluentValidation.Results;
using Pottencial.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Notifications
{
    public class BaseValidate : IBaseValidate
    {
        private readonly INotificator _notificator;

        public BaseValidate(INotificator notificador)
        {
            _notificator = notificador;
        }

        public void Notify(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Errors)
            {
                Notify(error.ErrorMessage);
            }
        }

        public void Notify(string mensagem)
        {
            _notificator.Handle(new Notification(mensagem));
        }

        public bool ValidOperation()
        {
            return !_notificator.ExistNotification();
        }

        public dynamic GetNotifications() => _notificator.GetNotifications().Select(n => n.Message);

        public bool ExecuteValidation<TV, TE>(TV validacao, TE entidade) where TV : AbstractValidator<TE>
        {
            var validator = validacao.Validate(entidade);

            if (validator.IsValid) return true;

            Notify(validator);

            return false;
        }

        public async Task<bool> ExecuteValidationAsync<TV, TE>(TV validacao, TE entidade) where TV : AbstractValidator<TE>
        {
            var validator = await validacao.ValidateAsync(entidade);

            if (validator.IsValid) return true;

            Notify(validator);

            return false;
        }

    }
}