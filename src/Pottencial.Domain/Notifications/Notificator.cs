﻿using Pottencial.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Notifications
{
    public class Notificator : INotificator
    {
        private List<Notification> _notificators;

        public Notificator() => _notificators = new List<Notification>();

        public void Handle(Notification notificacao) => _notificators.Add(notificacao);

        public List<Notification> GetNotifications() => _notificators;

        public string GetNotification(string message)
        {
            if (_notificators.Any())
                return _notificators.Where(x => x.Message == message).FirstOrDefault()!.Message;

            return null!;
        }

        public bool ExistNotification() => _notificators.Any();
    }
}
