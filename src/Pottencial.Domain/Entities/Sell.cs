﻿using Pottencial.Shareable.Enums;
using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Entities
{
    public class Sell
    {
        public long IdSell { get; private set; }
        public int IdStatus { get; private set; }
        public DateTime DhCreate { get; private set; } = DateTime.Now;
        public string Identifier { get; private set; } = string.Empty;
        public long IdSeller { get; private set; }

        [ForeignKey("IdSeller")]
        public Seller Seller { get; private set; }

        [ForeignKey("IdSell")]
        public List<Item> Items { get; private set; } = new List<Item>();

        public Sell()
        {

        }

        public Sell(string identifier, long idSeller, List<Item> items)
        {
            Identifier = identifier;
            IdStatus = (int)Enums.Status.AwaitingPayment;
            IdSeller = idSeller;
            Items = items;
        }

        public void SetSeller(Seller seller)
        {
            Seller = seller;
        }

        public void SetItems(List<Item> items)
        {
            Items = items;
        }

        public void SetStatus(int idStatus)
        {
            IdStatus = idStatus;
        }

        public void UpdateStatus(int idStatus)
        {
            IdStatus = idStatus;
        }

    }
}
