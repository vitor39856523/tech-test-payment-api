﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Entities
{
    public class Seller
    {
        public Seller()
        {

        }

        public Seller(string name, string cpf, string email, string telephone)
        {
            Name = name.Trim();
            Cpf = cpf;
            Email = email.Trim();
            Telephone = telephone;
        }

        public void SetId(long idSeller)
        {
            IdSeller = idSeller;
        }

        public long IdSeller { get; private set; }
        public string Name { get; private set; } = string.Empty;
        public string Cpf { get; private set; } = string.Empty;
        public string Email { get; private set; } = string.Empty;
        public string Telephone { get; private set; } = string.Empty;


    }
}
