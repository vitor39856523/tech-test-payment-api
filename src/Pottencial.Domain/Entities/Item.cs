﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Entities
{
    public class Item
    {
        public long IdItem { get; private set; }
        public string Name { get; private set; } = string.Empty;
        public DateTime DhCreate { get; private set; }
        public long IdSell { get; private set; }

        public Item(string name)
        {
            Name = name;
        }

        public void SetValues(long idItem, DateTime dhCreate, long idSell)
        {
            IdItem = idItem;
            DhCreate = dhCreate;
            IdSell = idSell;    
        }



    }
}
