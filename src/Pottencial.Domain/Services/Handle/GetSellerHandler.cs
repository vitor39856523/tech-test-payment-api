﻿using AutoMapper;
using MediatR;
using OperationResult;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Shareable.DomainObjects;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Services.Handle
{
    public class GetSellerHandler : IRequestHandler<GetSellerRequest, Result<GetSellerResponse>>
    {
        private readonly IRepositoryBase<Seller> _seller;
        private readonly IBaseValidate _baseValidate;

        public GetSellerHandler(IRepositoryBase<Seller> seller, IBaseValidate baseValidate)
        {
            _seller = seller;
            _baseValidate = baseValidate;
        }

        public async Task<Result<GetSellerResponse>> Handle(GetSellerRequest request,
            CancellationToken cancellationToken)
        {
            try
            {
                var sellers = await _seller.GetAllAsync();

                if (sellers == null)
                {
                    _baseValidate.Notify("Sellers not found, check and try again");
                    throw new DomainException();
                }

                return new GetSellerResponse(HandlerReponse.GetSeller(sellers));
            }
            catch (Exception ex)
            {
                return new DomainException(ex.Message);
            }
        }



    }
}
