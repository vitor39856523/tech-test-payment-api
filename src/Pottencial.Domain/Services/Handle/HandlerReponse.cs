﻿using Pottencial.Domain.Entities;
using Pottencial.Shareable.Enums;
using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Services.Handle
{
    public static class HandlerReponse
    {
        public static SellerRequest GetSeller(Seller seller)
        {
            return new SellerRequest(seller.IdSeller, seller.Name, seller.Cpf, seller.Email, seller.Telephone);
        }

        public static List<SellerRequest> GetSeller(List<Seller> sellers)
        {
            var sellersList = new List<SellerRequest>();

            foreach (var seller in sellers)
            {
                var s = new SellerRequest(seller.IdSeller, seller.Name, seller.Cpf, seller.Email, seller.Telephone);
                sellersList.Add(s);
            }

            return sellersList;
        }

        public static string GetStatus(int idStatus)
        {
            switch (idStatus)
            {
                case (int)Enums.Status.AwaitingPayment:
                    return "Awaiting Payment";

                case (int)Enums.Status.PaymentAccept:
                    return "Payment Accept";

                case (int)Enums.Status.SendTransporter:
                    return "Send Transporter";

                case (int)Enums.Status.Delivered:
                    return "Delivered";

                case (int)Enums.Status.Canceled:
                    return "Canceled";

                default:
                    return "Unknown";
            }
        }

        public static List<ItemRequest> GetItems(List<Item> items)
        {
            var itemsRequest = new List<ItemRequest>();

            foreach (var item in items)
                itemsRequest.Add(new ItemRequest { Name = item.Name });

            return itemsRequest;
        }

        public static List<Item> GetItems(List<ItemRequest> itemsRequest)
        {
            var items = new List<Item>();

            foreach (var i in itemsRequest)
                items.Add(new Item(i.Name));

            return items;
        }




    }
}
