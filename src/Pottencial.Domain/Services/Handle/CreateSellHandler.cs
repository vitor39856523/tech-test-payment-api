﻿using AutoMapper;
using MediatR;
using OperationResult;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Shareable.DomainObjects;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;
using Pottencial.Shareable.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Services.Handle
{
    public class CreateSellHandler : IRequestHandler<CreateSellRequest, Result<CreateSellResponse>>
    {
        private readonly IRepositoryBase<Sell> _repSell;
        private readonly IRepositoryBase<Seller> _repSeller;
        private readonly IBaseValidate _baseValidate;

        public CreateSellHandler(
            IRepositoryBase<Sell> sell,
            IBaseValidate baseValidate,
            IRepositoryBase<Seller> repSeller)
        {
            _repSell = sell;
            _baseValidate = baseValidate;
            _repSeller = repSeller;
        }

        public async Task<Result<CreateSellResponse>> Handle(CreateSellRequest request,
            CancellationToken cancellationToken)
        {
            try
            {
                if (!_baseValidate.ExecuteValidation(new CreateSellRequestValidator(), request))
                    throw new DomainException();

                var seller = await _repSeller.FindByAsync(request.IdSeller);

                if (seller == null)
                {
                    _baseValidate.Notify("Seller not found, check and try again");
                    throw new DomainException();
                }

                var sell = new Sell(request.Identifier, seller.IdSeller, HandlerReponse.GetItems(request.Items));
                sell = await _repSell.AddAsync(sell);

                return new CreateSellResponse(
                    request,
                    sell.DhCreate,
                    HandlerReponse.GetSeller(sell.Seller),
                    HandlerReponse.GetStatus(sell.IdStatus));
            }
            catch (Exception ex)
            {
                return new DomainException(ex.Message);
            }
        }



    }
}

