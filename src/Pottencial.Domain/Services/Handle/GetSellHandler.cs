﻿using MediatR;
using OperationResult;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Shareable.DomainObjects;
using Pottencial.Shareable.Enums;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;
using Pottencial.Shareable.Validators;

namespace Pottencial.Domain.Services.Handle
{
    public class GetSellHandler : IRequestHandler<GetSellRequest, Result<GetSellResponse>>
    {
        private readonly IBaseValidate _baseValidate;
        private readonly IRepositoryBase<Sell> _repSell;

        public GetSellHandler(IRepositoryBase<Sell> repSell, IBaseValidate baseValidate)
        {
            _repSell = repSell;
            _baseValidate = baseValidate;
        }

        public async Task<Result<GetSellResponse>> Handle(GetSellRequest request,
            CancellationToken cancellationToken)
        {
            try
            {
                if (!_baseValidate.ExecuteValidation(new GetSellRequestValidator(), request))
                    throw new DomainException();

                var sell = await _repSell.FindByAsyncSingle(x => x.IdSell == request.IdSell, "Seller,Items");

                if (sell == null)
                {
                    _baseValidate.Notify("Sell not found, check and try again");
                    throw new DomainException();
                }

                return new GetSellResponse
                    (sell.DhCreate,
                    HandlerReponse.GetSeller(sell.Seller), 
                    HandlerReponse.GetStatus(sell.IdStatus), sell.Identifier,
                    HandlerReponse.GetItems(sell.Items)
                    );
            }
            catch (Exception ex)
            {
                return new DomainException(ex.Message);
            }
        }



    }
}
