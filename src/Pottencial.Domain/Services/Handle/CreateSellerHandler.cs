﻿using FluentValidation;
using MediatR;
using OperationResult;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Domain.Notifications;
using Pottencial.Shareable.DomainObjects;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;
using Pottencial.Shareable.Validators;

namespace Pottencial.Domain.Services.Handle
{
    public class CreateSellerHandler : IRequestHandler<CreateSellerRequest, Result<CreateSellerResponse>>
    {
        private readonly IRepositoryBase<Seller> _seller;
        private readonly IBaseValidate _baseValidate;

        public CreateSellerHandler(IRepositoryBase<Seller> seller,
            IBaseValidate baseValidate)
        {
            _seller = seller;
            _baseValidate = baseValidate;
        }

        public async Task<Result<CreateSellerResponse>> Handle(CreateSellerRequest request,
            CancellationToken cancellationToken)
        {
            try
            {
                if (!_baseValidate.ExecuteValidation(new CreateSellerRequestValidator(), request))
                    throw new DomainException();

                var seller = new Seller(request.Nome, request.Cpf, request.Email, request.Telefone);
                await _seller.AddAsync(seller);

                return new CreateSellerResponse(HandlerReponse.GetSeller(seller));
            }
            catch (Exception ex)
            {
                return new DomainException(ex.Message);
            }
        }



    }
}
