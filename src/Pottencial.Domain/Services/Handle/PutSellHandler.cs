﻿using MediatR;
using OperationResult;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Shareable.DomainObjects;
using Pottencial.Shareable.Enums;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;
using Pottencial.Shareable.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using static Pottencial.Shareable.Enums.Enums;

namespace Pottencial.Domain.Services.Handle
{
    public class PutSellHandler : IRequestHandler<PutSellRequest, Result<GetSellResponse>>
    {
        private readonly IRepositoryBase<Sell> _sell;
        private readonly IBaseValidate _baseValidate;
        private readonly string paymentAccept = EnumHelper.GetEnumStringValue(Status.PaymentAccept);
        private readonly string sendTransporter = EnumHelper.GetEnumStringValue(Status.SendTransporter);
        private readonly string delivered = EnumHelper.GetEnumStringValue(Status.Delivered);
        private readonly string canceled = EnumHelper.GetEnumStringValue(Status.Canceled);

        public PutSellHandler(IRepositoryBase<Sell> sell,
            IBaseValidate baseValidate)
        {
            _sell = sell;
            _baseValidate = baseValidate;
        }

        public async Task<Result<GetSellResponse>> Handle(PutSellRequest request,
            CancellationToken cancellationToken)
        {
            try
            {
                if (!_baseValidate.ExecuteValidation(new PutSellRequestValidator(), request))
                    throw new DomainException();

                var sell = await _sell.FindByAsyncSingle(x => x.IdSell == request.IdSell, "Seller,Items");

                if (sell == null)
                {
                    _baseValidate.Notify("Sell not found, check and try again");
                    throw new DomainException();
                }

                request.Status = request.Status.Trim().ToUpper();
                
                var acceptedTransactions = new List<int>()
                { (int)Status.PaymentAccept, (int)Status.AwaitingPayment, (int)Status.SendTransporter };

                if (!acceptedTransactions.Contains(sell.IdStatus))
                {
                    _baseValidate.Notify("Transaction not accepted");
                    throw new DomainException();
                }

                if (
                sell.IdStatus == (int)Status.AwaitingPayment &&
                (request.Status != paymentAccept && request.Status != canceled)
                 )
                {
                    _baseValidate.Notify("Invalid status change");
                    throw new DomainException();
                }
                else if (
                sell.IdStatus == (int)Status.PaymentAccept &&
                (request.Status != sendTransporter && request.Status != canceled)
                )
                {
                    _baseValidate.Notify("Invalid status change");
                    throw new DomainException();
                }
                else if (sell.IdStatus == (int)Status.SendTransporter && request.Status != delivered)
                {
                    _baseValidate.Notify("Invalid status change");
                    throw new DomainException();
                }

                sell.UpdateStatus(EnumHelper.GetIdFromStatusString(request.Status));
                await _sell.UpdateAsync(sell);

                return new GetSellResponse
                    (sell.DhCreate,
                    HandlerReponse.GetSeller(sell.Seller),
                    HandlerReponse.GetStatus(sell.IdStatus), sell.Identifier,
                    HandlerReponse.GetItems(sell.Items)
                    );
            }
            catch (Exception ex)
            {
                return new DomainException(ex.Message);
            }
        }



    }
}
