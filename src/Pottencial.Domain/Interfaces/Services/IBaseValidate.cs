﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Interfaces.Services
{
    public interface IBaseValidate
    {
        void Notify(ValidationResult validationResult);
        void Notify(string mensagem);
        bool ExecuteValidation<TV, TE>(TV validacao, TE entidade) where TV : AbstractValidator<TE>;
        bool ValidOperation();
        Task<bool> ExecuteValidationAsync<TV, TE>(TV validacao, TE entidade) where TV : AbstractValidator<TE>;
        dynamic GetNotifications();
    }
}
