﻿using Pottencial.Domain.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Interfaces.Services
{
    public interface INotificator
    {
        bool ExistNotification();
        string GetNotification(string message);
        List<Notification> GetNotifications();
        void Handle(Notification notificacao);
    }
}