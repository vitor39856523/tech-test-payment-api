﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Interfaces.Repositories
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        #region ASYNC

        Task<List<TEntity>> GetAllAsync();
        Task<List<TEntity>> GetAllAsyncWithProperties(string includeProperties);
        Task<List<TEntity>> FindByAsyncList(Expression<Func<TEntity, bool>> filter);
        Task<List<TEntity>> FindByAsyncList(Expression<Func<TEntity, bool>> filter = null!, string includeProperties = "");
        Task<List<TEntity>> FindByAsyncList(Expression<Func<TEntity, bool>> filter = null!, Expression<Func<TEntity, DateTime>> order = null!, string orderType = "", string includeProperties = "", long take = 0);
        Task<List<TEntity>> FindByAsyncList(Expression<Func<TEntity, bool>> filter = null!, Expression<Func<TEntity, string>> order = null!, string orderType = "", string includeProperties = "", long take = 0);
        Task<List<TEntity>> FindByAsyncListWithoutAsNoTracking(Expression<Func<TEntity, bool>> filter = null!, string includeProperties = "");
        Task<TEntity> FindByAsyncSingle(Expression<Func<TEntity, bool>> filter, string includeProperties = "");
        Task<TEntity> FindByAsyncSingle(Expression<Func<TEntity, bool>> filter);
        Task<TEntity> FindByAsync(long id);
        Task<TEntity> FindByAsyncSingleWithAsNoTracking(Expression<Func<TEntity, bool>> filter, string includeProperties = "");
        Task<TEntity> AddAsync(TEntity toAdd);
        Task UpdateAsync(TEntity toUpdate);
        Task DeleteAsync(long id);
        Task DeleteAsync(TEntity entity);
        Task DeleteManyAsync(List<TEntity> entitys);
        Task<List<TEntity>> ExecSQLAsync(string Query);
        Task<TEntity> FindByAsyncSingleLast(Expression<Func<TEntity, bool>> filter, string includeProperties = "");
        Task<TEntity> FindByAsyncSingleFirst(Expression<Func<TEntity, bool>> filter, string includeProperties = "");
        Task<TEntity> FindByAsyncSingle(Expression<Func<TEntity, bool>> filter = null!, Expression<Func<TEntity, long>> order = null!,
        string orderType = "", string includeProperties = "");

        #endregion

    }
}
