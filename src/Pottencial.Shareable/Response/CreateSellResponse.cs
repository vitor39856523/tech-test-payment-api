﻿using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Response
{
    public class CreateSellResponse
    {
        public DateTime DhCreate { get; set; } = DateTime.Now;
        public string Identifier { get; set; } = string.Empty;
        public SellerRequest Seller { get; set; } = new SellerRequest();
        public StatusRequest Status { get; set; } = new StatusRequest();    
        public List<ItemRequest> Items { get; set; } = new List<ItemRequest>();

        public CreateSellResponse(CreateSellRequest request, DateTime dhCreate, SellerRequest seller, string status)
        {
            foreach (var item in request.Items)
                Items.Add(new ItemRequest { Name = item.Name });

            Identifier = request.Identifier;
            DhCreate = dhCreate;
            Seller = seller;
            Status = new StatusRequest(status);
        }

    }
}
