﻿using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Response
{
    public class GetSellResponse
    {
        public DateTime DhCreate { get; set; } = DateTime.Now;
        public string Identifier { get; set; } = string.Empty;
        public SellerRequest Seller { get; set; } = new SellerRequest();
        public StatusRequest Status { get; set; } = new StatusRequest();
        public List<ItemRequest> Items { get; set; } = new List<ItemRequest>();

        public GetSellResponse(DateTime dhCreate, SellerRequest seller, string status, string identifier, List<ItemRequest> items)
        {
            Identifier = identifier;
            DhCreate = dhCreate;
            Seller = seller;
            Status = new StatusRequest(status);
            Items = items;
        }

    }
}


