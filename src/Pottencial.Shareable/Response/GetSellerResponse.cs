﻿using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Response
{
    public class GetSellerResponse
    {
        public List<SellerRequest> Sellers { get; set; }

        public GetSellerResponse(List<SellerRequest> sellers)
        {
            Sellers = sellers;
        }
    }
}
