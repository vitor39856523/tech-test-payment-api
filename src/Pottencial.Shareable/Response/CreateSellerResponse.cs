﻿using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Response
{
    public class CreateSellerResponse
    {
        public SellerRequest SellerRequest { get; set; }

        public CreateSellerResponse(SellerRequest sellerRequest)
        {
            SellerRequest = sellerRequest;
        }
    }
}
