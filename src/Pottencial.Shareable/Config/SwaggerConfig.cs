﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Config
{
    public static class SwaggerConfig
    {
        public static void SetSettingsSwagger(IServiceCollection services, IConfiguration Configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Pottencial.API",
                    Version = "v1",
                    Description = "Esta API é um teste da empresa Pottencial para vaga de Dev | " +
                    " Descrição de status para alterar venda: " +
                    " AWAITING PAYMENT -" +
                    " PAYMENT ACCEPT -" +
                    " DELIVERED -" +
                    " CANCELED -" +
                    " SEND TRANSPORTER ",
                });

            });
        }

        public static void SetVersionAPISwagger(WebApplication app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pottencial.API - v1");
            });
        }




    }
}
