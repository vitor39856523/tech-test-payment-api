﻿using MediatR;
using OperationResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Request
{
    public class SellerRequest : IRequest<Result<object>>
    {
        public SellerRequest()
        {
                
        }

        public long IdSeller { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Cpf { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Telephone { get; set; } = string.Empty;

        public SellerRequest(long idSeller, string name, string cpf, string email, string telephone)
        {
            IdSeller = idSeller;
            Name = name;
            Cpf = cpf;
            Email = email;
            Telephone = telephone;
        }

    }
}
