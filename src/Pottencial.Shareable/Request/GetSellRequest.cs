﻿using MediatR;
using OperationResult;
using Pottencial.Shareable.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Request
{
    public class GetSellRequest : IRequest<Result<GetSellResponse>>
    {
        public long IdSell { get; set; }
    }
}

