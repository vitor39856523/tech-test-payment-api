﻿using MediatR;
using OperationResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Request
{
    public class ItemRequest : IRequest<Result<object>>
    {
        public string Name { get; set; } = string.Empty;
    }
}
