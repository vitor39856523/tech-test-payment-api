﻿using MediatR;
using OperationResult;
using Pottencial.Shareable.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Request
{
    public class CreateSellerRequest : IRequest<Result<CreateSellerResponse>>
    {
        public string Nome { get; set; } = string.Empty;
        public string Cpf { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Telefone { get; set; } = string.Empty;
    }
}
