﻿using AutoMapper.Configuration.Annotations;
using MediatR;
using OperationResult;
using Pottencial.Shareable.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Request
{
    public class PutSellRequest : IRequest<Result<GetSellResponse>>
    {
        [JsonIgnore]
        public long IdSell { get; set; }
        public string Status { get; set; }
    }
}
