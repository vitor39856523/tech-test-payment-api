﻿using MediatR;
using OperationResult;
using Pottencial.Shareable.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Request
{
    public class CreateSellRequest : IRequest<Result<CreateSellResponse>>
    {
        public string Identifier { get; set; } = string.Empty;
        public long IdSeller { get; set; }

        public List<ItemRequest> Items { get; set; } = new List<ItemRequest>();


    }
}
