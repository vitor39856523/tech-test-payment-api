﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Pottencial.Shareable.Enums.EnumHelper;

namespace Pottencial.Shareable.Enums
{
    public static class Enums
    {
        public enum Status
        {
            [EnumStringValue("AWAITING PAYMENT")]
            AwaitingPayment = 1,

            [EnumStringValue("PAYMENT ACCEPT")]
            PaymentAccept = 2,

            [EnumStringValue("SEND TRANSPORTER")]
            SendTransporter = 3,

            [EnumStringValue("DELIVERED")]
            Delivered = 4,

            [EnumStringValue("CANCELED")]
            Canceled = 5,
        }




    }
}
