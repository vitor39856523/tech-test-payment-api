﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Pottencial.Shareable.Enums.Enums;

namespace Pottencial.Shareable.Enums
{
    public static class EnumHelper
    {
        public class EnumStringValue : Attribute
        {
            private string _value;

            public EnumStringValue(string value)
            {
                _value = value;
            }

            public string Value
            {
                get { return _value; }
            }
        }

        public static string GetEnumStringValue(Enum value)
        {
            string output = string.Empty;
            Type type = value.GetType();

            FieldInfo fi = type.GetField(value.ToString());

            EnumStringValue[] attrs = fi.GetCustomAttributes(typeof(EnumStringValue), false) as EnumStringValue[];

            if (attrs.Length > 0)
            {
                output = attrs[0].Value;
            }

            return output;
        }

        public static int GetIdFromStatusString(string statusString)
        {
            foreach (Status status in Enum.GetValues(typeof(Status)))
            {
                FieldInfo fieldInfo = status.GetType().GetField(status.ToString());
                EnumStringValue[]? attributes =
                    fieldInfo.GetCustomAttributes(typeof(EnumStringValue), false) as EnumStringValue[];

                if (attributes != null && attributes.Length > 0 && attributes[0].Value.Equals(statusString))
                    return (int)status;
            }

            return 0;
        }


    }
}
