﻿using FluentValidation;
using Pottencial.Shareable.Enums;
using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Pottencial.Shareable.Enums.Enums;

namespace Pottencial.Shareable.Validators
{
    public class PutSellRequestValidator : AbstractValidator<PutSellRequest>
    {
        public PutSellRequestValidator()
        {
            RuleFor(x => x.IdSell)
                .Must(x => x > 0)
                .WithMessage(string.Format("IdSell invalid",
                nameof(PutSellRequest.IdSell)));

            RuleFor(x => x).Custom((x, context) =>
            {
                if (string.IsNullOrWhiteSpace(x.Status))
                {
                    context.AddFailure(string.Format(" The property {0} is not be empty ", nameof(PutSellRequest.Status)));
                }
                else
                {
                    var status = x.Status.Trim().ToUpper();

                    var awaitingPayment = EnumHelper.GetEnumStringValue(Enums.Enums.Status.AwaitingPayment);
                    var paymentAccept = EnumHelper.GetEnumStringValue(Enums.Enums.Status.PaymentAccept);
                    var sendTransporter = EnumHelper.GetEnumStringValue(Enums.Enums.Status.SendTransporter);
                    var delivered = EnumHelper.GetEnumStringValue(Enums.Enums.Status.Delivered);
                    var canceled = EnumHelper.GetEnumStringValue(Enums.Enums.Status.Canceled);

                    if (
                    status != awaitingPayment
                    &&
                    status != paymentAccept
                    &&
                    status != sendTransporter
                    &&
                    status != delivered
                    &&
                    status != canceled
                    )
                    {
                        context.AddFailure("Status Invalid");
                    }
                }
            });
        }
    }
}
