﻿using FluentValidation;
using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Validators
{
    public class CreateSellRequestValidator : AbstractValidator<CreateSellRequest>
    {
        public CreateSellRequestValidator()
        {
            RuleFor(x => x.IdSeller)
                .Must(x => x > 0)
                .WithMessage(string.Format("IdSeller invalid",
                nameof(CreateSellRequest.IdSeller)));

            RuleFor(x => x.Identifier)
              .NotEmpty()
              .WithMessage(string.Format("The property '{0}' is not be empty",
              nameof(CreateSellRequest.Identifier)));

            RuleFor(x => x).Custom((x, context) =>
            {
                if (x.Items == null || !x.Items.Any())
                {
                    context.AddFailure(string.Format("'{0}': Item is required to create sale", nameof(CreateSellRequest.Items)));
                }
                else
                {
                    if (x.Items.Where(x => string.IsNullOrWhiteSpace(x.Name)).Any())
                    {
                        context.AddFailure(string.Format("'{0}': Name is required to create item", nameof(CreateSellRequest.Items)));
                    }
                }
            });

        }
    }
}
