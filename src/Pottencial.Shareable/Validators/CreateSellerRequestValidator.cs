﻿using FluentValidation;
using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Validators
{
    public class CreateSellerRequestValidator : AbstractValidator<CreateSellerRequest>
    {
        public CreateSellerRequestValidator()
        {
            RuleFor(x => x.Cpf)
                .NotEmpty().WithMessage(string.Format("The property {0} is not be empty",
                nameof(CreateSellerRequest.Cpf)))
                .IsValidCPF().WithMessage("CPF invalid");

            RuleFor(x => x.Nome)
                .NotEmpty()
                .WithMessage(string.Format("The property {0} is not be empty",
                nameof(CreateSellerRequest.Nome)));

            RuleFor(x => x.Email)
               .NotEmpty()
               .WithMessage(string.Format("The property {0} is not be empty",
               nameof(CreateSellerRequest.Email)))

              .EmailAddress().WithMessage("Email invalid");

            RuleFor(x => x.Telefone)
                .NotEmpty()
                .WithMessage(string.Format("The property {0} is not be empty",
                nameof(CreateSellerRequest.Telefone)));
        }
    }
}
