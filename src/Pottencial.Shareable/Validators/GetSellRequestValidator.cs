﻿using FluentValidation;
using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Shareable.Validators
{
    public class GetSellRequestValidator : AbstractValidator<GetSellRequest>
    {
        public GetSellRequestValidator()
        {
            RuleFor(x => x.IdSell)
                .Must(x => x > 0).WithMessage("IdSell invalid");
        }
    }
}
