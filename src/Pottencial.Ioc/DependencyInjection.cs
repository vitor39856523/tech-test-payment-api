﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Pottencial.Domain;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Domain.Notifications;
using Pottencial.Infra.Context;
using Pottencial.Infra.Repositories;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Validators;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Pottencial.Shareable.Config.SwaggerConfig;

namespace Pottencial.Ioc
{
    public static class DependencyInjection
    {
        public static void RegisterServices(this IServiceCollection services,
            IConfiguration Configuration)
        {
            services.AddScoped<IRepositoryBase<Item>, RepositoryBase<Item>>();
            services.AddScoped<IRepositoryBase<Sell>, RepositoryBase<Sell>>();
            services.AddScoped<IRepositoryBase<Seller>, RepositoryBase<Seller>>();

            services.AddScoped<IValidator<CreateSellerRequest>, CreateSellerRequestValidator>();

            services.AddScoped<IBaseValidate, BaseValidate>();
            services.AddScoped<INotificator, Notificator>();

            services.AddMediatR(cfg =>
            {
                cfg.RegisterServicesFromAssembly(typeof(DomainEntry).Assembly);
            });

            ReturnSetConnection(services, Configuration);
        }

        public static void ReturnSetConnection(IServiceCollection services,
            IConfiguration Configuration)
        {
            services.AddDbContext<ApplicationDbContext>();
        }





    }
}
