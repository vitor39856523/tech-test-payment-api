﻿using FluentValidation.Results;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Pottencial.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Pottencial.Infra.Context;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Pottencial.Infra.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public RepositoryBase(ApplicationDbContext applicationDbContext)
        {
            if (!applicationDbContext.Database.IsInMemory())
                applicationDbContext.Database.Migrate();

            _context = applicationDbContext;
            _dbSet = _context.Set<TEntity>();

        }

        #region ASYNC
        public async Task<List<TEntity>> GetAllAsync()
        {
            return await _dbSet.AsNoTracking().ToListAsync();
        }

        public async Task<List<TEntity>> GetAllAsyncWithProperties(string includeProperties)
        {
            IQueryable<TEntity> query = _dbSet;

            foreach (string includeProperty in includeProperties.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<List<TEntity>> FindByAsyncList(Expression<Func<TEntity, bool>> filter = null!, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();


            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<List<TEntity>> FindByAsyncList(Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<List<TEntity>> FindByAsyncList(Expression<Func<TEntity, bool>> filter = null!, Expression<Func<TEntity, DateTime>> order = null!,
            string orderType = "", string includeProperties = "", long take = 0)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();

            if (!string.IsNullOrWhiteSpace(orderType) && orderType == "DESC")
                query = query.OrderByDescending(order);
            else if (!string.IsNullOrWhiteSpace(orderType) && orderType == "ASC")
                query = query.OrderBy(order);

            if (take > 0)
                return await query.Take((int)take).AsNoTracking().ToListAsync();

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<List<TEntity>> FindByAsyncList(Expression<Func<TEntity, bool>> filter = null!, Expression<Func<TEntity, string>> order = null!,
            string orderType = "", string includeProperties = "", long take = 0)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();

            if (!string.IsNullOrWhiteSpace(orderType) && orderType == "DESC")
                query = query.OrderByDescending(order);
            else if (!string.IsNullOrWhiteSpace(orderType) && orderType == "ASC")
                query = query.OrderBy(order);

            if (take > 0)
                return await query.Take((int)take).AsNoTracking().ToListAsync();

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task<List<TEntity>> FindByAsyncListWithoutAsNoTracking(Expression<Func<TEntity, bool>> filter = null!, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();


            return await query.ToListAsync();
        }

        public async Task<TEntity> FindByAsyncSingle(Expression<Func<TEntity, bool>> filter, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();

            return await query.FirstOrDefaultAsync();
        }

        public async Task<TEntity> FindByAsyncSingle(Expression<Func<TEntity, bool>> filter = null!, Expression<Func<TEntity, long>> order = null!,
        string orderType = "", string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();

            if (!string.IsNullOrWhiteSpace(orderType) && orderType == "DESC")
                query = query.OrderByDescending(order);
            else if (!string.IsNullOrWhiteSpace(orderType) && orderType == "ASC")
                query = query.OrderBy(order);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<TEntity> FindByAsyncSingleFirst(Expression<Func<TEntity, bool>> filter, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();


            return await query.FirstOrDefaultAsync();
        }

        public async Task<TEntity> FindByAsyncSingleLast(Expression<Func<TEntity, bool>> filter, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();


            return await query.LastOrDefaultAsync();
        }

        public async Task<TEntity> FindByAsyncSingle(Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<TEntity> FindByAsyncSingleWithAsNoTracking(Expression<Func<TEntity, bool>> filter, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty).AsSplitQuery();

            return await query.AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<TEntity> FindByAsync(long id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public async Task<TEntity> AddAsync(TEntity toAdd)
        {
            await _dbSet.AddAsync(toAdd);
            await _context.SaveChangesAsync();

            return toAdd;
        }

        public async Task UpdateAsync(TEntity toUpdate)
        {
            //_dbSet.Attach(toUpdate);
            _context.Entry(toUpdate).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(long id)
        {
            TEntity entity = _dbSet.Find(id)!;
            _dbSet.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            _dbSet.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteManyAsync(List<TEntity> entitys)
        {
            foreach (var obj in entitys)
                _dbSet.Remove(obj);

            await _context.SaveChangesAsync();
        }

        public async Task<List<TEntity>> ExecSQLAsync(string Query)
        {
            var result = await _context.Set<TEntity>().FromSqlRaw(Query).ToListAsync();
            return result;
        }


        #endregion

    }
}
