﻿using Moq;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Domain.Notifications;
using Pottencial.Domain.Services.Handle;
using Pottencial.Tests.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Pottencial.Tests
{
    public class GetSellHandlerTests
    {
        private readonly Mock<IRepositoryBase<Sell>> mockSell;
        private readonly IBaseValidate baseValidate;
        private readonly Mock<IRepositoryBase<Seller>> mockSeller;
        private GetSellHandler GetSellHandler;
        private readonly INotificator notificator;

        public GetSellHandlerTests()
        {
            notificator = new Notificator();
            mockSell = new Mock<IRepositoryBase<Sell>>();
            baseValidate = new BaseValidate(notificator);
            mockSeller = new Mock<IRepositoryBase<Seller>>();
            GetSellHandler = new GetSellHandler(mockSell.Object, baseValidate);
        }

        [Fact]
        public async Task Handle_ValidRequest_ReturnsValidResponse()
        {
            var request = new GetSellRequestBuilder().Build();

            mockSell
           .Setup(x => x.FindByAsyncSingle(It.IsAny<Expression<Func<Sell, bool>>>(), It.IsAny<string>()))
           .ReturnsAsync(new SellBuilder().Build());

            GetSellHandler = new GetSellHandler(
             mockSell.Object,
             baseValidate
            );

            var result = await GetSellHandler.Handle(request, CancellationToken.None);

            Assert.True(!notificator.GetNotifications().Any());
            Assert.True(result.IsSuccess);
            Assert.Null(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_ReturnsException()
        {
            var request = new GetSellRequestBuilder().WithIdSell(0).Build();

            GetSellHandler = new GetSellHandler(
             mockSell.Object,
             baseValidate
            );

            var result = await GetSellHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_NotFound()
        {
            var request = new GetSellRequestBuilder().WithIdSell(20).Build();

            GetSellHandler = new GetSellHandler(
             mockSell.Object,
             baseValidate
            );

            var result = await GetSellHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }






    }
}