using FluentValidation;
using Moq;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Domain.Notifications;
using Pottencial.Domain.Services.Handle;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Validators;
using Pottencial.Tests.Builders;
using System.Linq;
using Xunit;

namespace Pottencial.Tests
{
    public class CreateSellerHandlerTests
    {
        private readonly Mock<IRepositoryBase<Seller>> repository;
        private CreateSellerHandler createSellerHandler;
        private readonly IBaseValidate baseValidate;
        private readonly INotificator notificator;

        public CreateSellerHandlerTests()
        {
            repository = new Mock<IRepositoryBase<Seller>>();
            notificator = new Notificator();
            baseValidate = new BaseValidate(notificator);
            createSellerHandler = new CreateSellerHandler(repository.Object, baseValidate);
        }

        [Fact]
        public async Task Handle_ValidRequest_ReturnsResult()
        {
            var request = new CreateSellerRequestBuilder().Build();

            repository
                .Setup(x => x.AddAsync(It.IsAny<Seller>()))
                .ReturnsAsync(new SellerBuilder().Build());

            createSellerHandler = new CreateSellerHandler(repository.Object, baseValidate);

            var result = await createSellerHandler.Handle(request, CancellationToken.None);

            Assert.True(!notificator.GetNotifications().Any());
            Assert.True(result.IsSuccess);
            Assert.Null(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_ReturnsException()
        {
            var request = new CreateSellerRequestBuilder().WithCpf("59865").Build();

            await createSellerHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
        }

    }
}