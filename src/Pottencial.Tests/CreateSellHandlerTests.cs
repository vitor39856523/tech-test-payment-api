﻿using AutoMapper;
using Moq;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Domain.Notifications;
using Pottencial.Domain.Services.Handle;
using Pottencial.Shareable.Validators;
using Pottencial.Tests.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Pottencial.Tests
{
    public class CreateSellHandlerTests
    {
        private readonly Mock<IRepositoryBase<Sell>> mockSell;
        private readonly IBaseValidate baseValidate;
        private readonly Mock<IRepositoryBase<Seller>> mockSeller;
        private CreateSellHandler createSellHandler;
        private readonly INotificator notificator;

        public CreateSellHandlerTests()
        {
            notificator = new Notificator();
            mockSell = new Mock<IRepositoryBase<Sell>>();
            baseValidate = new BaseValidate(notificator);
            mockSeller = new Mock<IRepositoryBase<Seller>>();
            createSellHandler = new CreateSellHandler(mockSell.Object, baseValidate, mockSeller.Object);
        }

        [Fact]
        public async Task Handle_ValidRequest_ReturnsValidResponse()
        {
            var request = new CreateSellRequestBuilder().Build();

            var seller = new SellerBuilder().Build();
            mockSeller.Setup(x => x.FindByAsync(It.IsAny<long>())).ReturnsAsync(seller);

            var sell = new SellBuilder().Build();
            mockSell.Setup(x => x.AddAsync(It.IsAny<Sell>())).ReturnsAsync(sell);

            createSellHandler = new CreateSellHandler(
             mockSell.Object,
             baseValidate,
             mockSeller.Object
            );

            var result = await createSellHandler.Handle(request, CancellationToken.None);

            Assert.True(!notificator.GetNotifications().Any());
            Assert.True(result.IsSuccess);
            Assert.Null(result.Exception);
        }

        [Fact]
        public async Task Handle_SellerNotFound_ReturnsException()
        {
            var request = new CreateSellRequestBuilder().Build();

            mockSeller.Setup(x => x.FindByAsync(It.IsAny<long>())).ReturnsAsync((Seller)null!);

            createSellHandler = new CreateSellHandler(
             mockSell.Object,
             baseValidate,
             mockSeller.Object
            );

            var result = await createSellHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_ReturnsException()
        {
            var request = new CreateSellRequestBuilder().WithIdSeller(0).Build();

            createSellHandler = new CreateSellHandler(
             mockSell.Object,
             baseValidate,
             mockSeller.Object
            );

            var result = await createSellHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }




    }
}