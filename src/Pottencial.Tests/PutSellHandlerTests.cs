﻿using Moq;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Domain.Notifications;
using Pottencial.Domain.Services.Handle;
using Pottencial.Shareable.Enums;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;
using Pottencial.Tests.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static Pottencial.Shareable.Enums.Enums;

namespace Pottencial.Tests
{
    public class PutSellHandlerTests
    {
        private readonly Mock<IRepositoryBase<Sell>> mockSell;
        private readonly IBaseValidate baseValidate;
        private PutSellHandler putSellHandler;
        private readonly INotificator notificator;

        public PutSellHandlerTests()
        {
            notificator = new Notificator();
            mockSell = new Mock<IRepositoryBase<Sell>>();
            baseValidate = new BaseValidate(notificator);
            putSellHandler = new PutSellHandler(mockSell.Object, baseValidate);
        }

        [Fact]
        public async Task Handle_ValidRequest_ReturnsValidResponse()
        {
            var request = new PutSellRequestBuilder().Build();

            mockSell
           .Setup(x => x.FindByAsyncSingle(It.IsAny<Expression<Func<Sell, bool>>>(), It.IsAny<string>()))
           .ReturnsAsync(new SellBuilder().Build());

            putSellHandler = new PutSellHandler(
             mockSell.Object,
             baseValidate
            );

            var result = await putSellHandler.Handle(request, CancellationToken.None);

            Assert.True(!notificator.GetNotifications().Any());
            Assert.True(result.IsSuccess);
            Assert.Null(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_StatusInvalid()
        {
            var request = new PutSellRequestBuilder()
                .WithStatus("test invalid").Build();

            mockSell
           .Setup(x => x.FindByAsyncSingle(It.IsAny<Expression<Func<Sell, bool>>>(), It.IsAny<string>()))
           .ReturnsAsync(new SellBuilder().Build());

            putSellHandler = new PutSellHandler(
             mockSell.Object,
             baseValidate
            );

            var result = await putSellHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_SellNotFound()
        {
            var request = new PutSellRequestBuilder().Build();

            putSellHandler = new PutSellHandler(
             mockSell.Object,
             baseValidate
            );

            var result = await putSellHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_TransactionNotAccepted()
        {
            var request = new PutSellRequestBuilder().Build();

             mockSell
            .Setup(x => x.FindByAsyncSingle(It.IsAny<Expression<Func<Sell, bool>>>(), It.IsAny<string>()))
            .ReturnsAsync(new SellBuilder().WithIdStatus((int)Status.Delivered).Build());

            putSellHandler = new PutSellHandler(
             mockSell.Object,
             baseValidate
            );

            var result = await putSellHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_InvalidStatusChange()
        {
            var request = new PutSellRequestBuilder()
                .WithStatus(EnumHelper.GetEnumStringValue(Status.SendTransporter)).Build();

            mockSell
           .Setup(x => x.FindByAsyncSingle(It.IsAny<Expression<Func<Sell, bool>>>(), It.IsAny<string>()))
           .ReturnsAsync(new SellBuilder().WithIdStatus((int)Status.AwaitingPayment).Build());

            putSellHandler = new PutSellHandler(
             mockSell.Object,
             baseValidate
            );

            var result = await putSellHandler.Handle(request, CancellationToken.None);

            Assert.True(notificator.GetNotifications().Any());
            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }

    }
}
