﻿using Moq;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Interfaces.Repositories;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Domain.Notifications;
using Pottencial.Domain.Services.Handle;
using Pottencial.Shareable.Request;
using Pottencial.Tests.Builders;
using Xunit;

namespace Pottencial.Tests
{
    public class GetSellerHandlerTests
    {
        private GetSellerHandler getSellerHandle;
        private readonly INotificator notificator;
        private readonly IBaseValidate baseValidate;
        private readonly Mock<IRepositoryBase<Seller>> mockSeller;

        public GetSellerHandlerTests()
        {
            notificator = new Notificator();
            baseValidate = new BaseValidate(notificator);
            mockSeller = new Mock<IRepositoryBase<Seller>>();
            getSellerHandle = new GetSellerHandler(mockSeller.Object, baseValidate);
        }

        [Fact]
        public async Task Handle_ValidRequest_ReturnsValidResponse()
        {
            var request = new GetSellerRequest();

            mockSeller.Setup(x => x.GetAllAsync()).ReturnsAsync(new SellerBuilder().BuildList());

            getSellerHandle = new GetSellerHandler(mockSeller.Object, baseValidate);

            var result = await getSellerHandle.Handle(request, CancellationToken.None);

            Assert.True(result.IsSuccess);
            Assert.Null(result.Exception);
        }

        [Fact]
        public async Task Handle_InvalidRequest_ReturnsException()
        {
            var request = new GetSellerRequest();

            getSellerHandle = new GetSellerHandler(mockSeller.Object, baseValidate);

            var result = await getSellerHandle.Handle(request, CancellationToken.None);

            Assert.False(result.IsSuccess);
            Assert.NotNull(result.Exception);
        }

    }
}
