
using Pottencial.Shareable.Request;

namespace Pottencial.Tests.Builders
{
    public class CreateSellRequestBuilder
    {
        private string _identifier;
        private long _idSeller;
        private List<ItemRequest> _items;

        public CreateSellRequestBuilder()
        {
            _identifier = Guid.NewGuid().ToString();
            _idSeller = 1;
            _items = new List<ItemRequest> { new ItemRequestBuilder().Build() };
        }

        public CreateSellRequestBuilder WithIdentifier(string identifier)
        {
            _identifier = identifier;
            return this;
        }

        public CreateSellRequestBuilder WithIdSeller(long idSeller)
        {
            _idSeller = idSeller;
            return this;
        }

        public CreateSellRequestBuilder WithItems(List<ItemRequest> items)
        {
            _items = items;
            return this;
        }

        public CreateSellRequest Build()
        {
            return new CreateSellRequest
            {
                Identifier = _identifier,
                IdSeller = _idSeller,
                Items = _items
            };
        }
    }
}
