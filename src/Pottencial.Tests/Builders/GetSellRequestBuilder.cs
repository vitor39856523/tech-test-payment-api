using MediatR;
using OperationResult;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Tests.Builders
{
    public class GetSellRequestBuilder
    {
        private long _idSell;

        public GetSellRequestBuilder()
        {
            _idSell = 1;
        }

        public GetSellRequestBuilder WithIdSell(long idSell)
        {
            _idSell = idSell;
            return this;
        }

        public GetSellRequest Build()
        {
            return new GetSellRequest
            {
                IdSell = _idSell
            };
        }
    }
}
