using Pottencial.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Tests.Builders
{
    public class SellerBuilder
    {
        private long _id;
        private string _nome;
        private string _cpf;
        private string _email;
        private string _telefone;

        public SellerBuilder()
        {
            _id = 1;
            _nome = "Vitor";
            _cpf = "54565465";
            _email = "vitorg_98@outlook.com";
            _telefone = "39856523";
        }

        public SellerBuilder WithId(long id)
        {
            _id = id;
            return this;
        }

        public SellerBuilder WithNome(string nome)
        {
            _nome = nome;
            return this;
        }

        public SellerBuilder WithCpf(string cpf)
        {
            _cpf = cpf;
            return this;
        }

        public SellerBuilder WithEmail(string email)
        {
            _email = email;
            return this;
        }

        public SellerBuilder WithTelefone(string telefone)
        {
            _telefone = telefone;
            return this;
        }

        public Seller Build()
        {
            var seller = new Seller(_nome, _cpf, _email, _telefone);

            seller.SetId(_id);
            
            return seller;
        }

        public List<Seller> BuildList()
        {
            var list = new List<Seller>();  

            var seller = new Seller(_nome, _cpf, _email, _telefone);
            var seller2 = new Seller(_nome, _cpf, _email, _telefone);

            seller.SetId(_id);
            seller2.SetId(_id);

            list.Add(seller);
            list.Add(seller2);

            return list;
        }
    }
}
