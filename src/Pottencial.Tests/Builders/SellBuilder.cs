using Pottencial.Domain.Entities;
using Pottencial.Shareable.Enums;
using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Tests.Builders
{
    public class SellBuilder
    {
        private long _idSell;
        private int _idStatus;
        private DateTime _dhCreate;
        private string _identifier;
        private long _idSeller;
        private Seller _seller;
        private List<Item> _items;

        public SellBuilder()
        {
            _idSell = 1;
            _idStatus = 1;
            _dhCreate = DateTime.Now;
            _identifier = Guid.NewGuid().ToString();
            _idSeller = 1;
            _seller = new SellerBuilder().Build();
            _items = new List<Item> { new ItemBuilder().Build() };
        }

        public SellBuilder WithIdSell(long idSell)
        {
            _idSell = idSell;
            return this;
        }

        public SellBuilder WithIdStatus(int idStatus)
        {
            _idStatus = idStatus;
            return this;
        }

        public SellBuilder WithDhCreate(DateTime dhCreate)
        {
            _dhCreate = dhCreate;
            return this;
        }

        public SellBuilder WithIdentifier(string identifier)
        {
            _identifier = identifier;
            return this;
        }

        public SellBuilder WithIdSeller(long idSeller)
        {
            _idSeller = idSeller;
            return this;
        }

        public SellBuilder WithSeller(Seller seller)
        {
            _seller = seller;
            return this;
        }

        public SellBuilder WithItems(List<Item> items)
        {
            _items = items;
            return this;
        }

        public Sell Build()
        {
            var sell = new Sell(_identifier, _idSeller, _items);

            sell.SetSeller(_seller);
            sell.SetItems(_items);
            sell.SetStatus(_idStatus);

            return sell;
        }
    }
}
