using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Domain.Entities
{
	public class ItemBuilder
	{
		private long _idItem;
		private string _name;
		private DateTime _dhCreate;
		private long _idSell;

		public ItemBuilder() { }

		public ItemBuilder WithIdItem(long idItem)
		{
			_idItem = idItem;
			return this;
		}

		public ItemBuilder WithName(string name)
		{
			_name = name;
			return this;
		}

		public ItemBuilder WithDhCreate(DateTime dhCreate)
		{
			_dhCreate = dhCreate;
			return this;
		}

		public ItemBuilder WithIdSell(long idSell)
		{
			_idSell = idSell;
			return this;
		}

		public Item Build()
		{
			var item = new Item(_name);

			item.SetValues(_idItem, _dhCreate, _idSell);

			return item;
		}
	}
}
