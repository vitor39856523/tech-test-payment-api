using MediatR;
using OperationResult;
using Pottencial.Shareable.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Tests.Builders
{
    public class ItemRequestBuilder
    {
        private string _name;

        public ItemRequestBuilder()
        {
            _name = "Placa mae";
        }

        public ItemRequestBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public ItemRequest Build()
        {
            return new ItemRequest
            {
                Name = _name
            };
        }
    }
}
