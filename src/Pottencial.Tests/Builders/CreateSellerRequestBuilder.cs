using MediatR;
using OperationResult;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Pottencial.Tests.Builders
{
    public class CreateSellerRequestBuilder
    {
        private string _nome;
        private string _cpf;
        private string _email;
        private string _telefone;

        public CreateSellerRequestBuilder()
        {
            _nome = "Vitor";
            _cpf = "49182180830";
            _email = "vitorg_98@outlook.com";
            _telefone = "39856523";
        }

        public CreateSellerRequestBuilder WithNome(string nome)
        {
            _nome = nome;
            return this;
        }

        public CreateSellerRequestBuilder WithCpf(string cpf)
        {
            _cpf = cpf;
            return this;
        }

        public CreateSellerRequestBuilder WithEmail(string email)
        {
            _email = email;
            return this;
        }

        public CreateSellerRequestBuilder WithTelefone(string telefone)
        {
            _telefone = telefone;
            return this;
        }

        public CreateSellerRequest Build()
        {
            return new CreateSellerRequest
            {
                Nome = _nome,
                Cpf = _cpf,
                Email = _email,
                Telefone = _telefone
            };
        }
    }
}
