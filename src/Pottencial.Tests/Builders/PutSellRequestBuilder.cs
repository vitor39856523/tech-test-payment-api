using AutoMapper.Configuration.Annotations;
using MediatR;
using OperationResult;
using Pottencial.Shareable.Enums;
using Pottencial.Shareable.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using static Pottencial.Shareable.Enums.Enums;

namespace Pottencial.Shareable.Request
{
    public class PutSellRequestBuilder
    {
        private long _idSell;
        private string _status;

        public PutSellRequestBuilder()
        {
            _idSell = 1;
            _status = EnumHelper.GetEnumStringValue(Status.PaymentAccept); 
        }

        public PutSellRequestBuilder WithIdSell(long idSell)
        {
            _idSell = idSell;
            return this;
        }

        public PutSellRequestBuilder WithStatus(string status)
        {
            _status = status;
            return this;
        }

        public PutSellRequest Build()
        {
            return new PutSellRequest
            {
                IdSell = _idSell,
                Status = _status
            };
        }
    }
}
