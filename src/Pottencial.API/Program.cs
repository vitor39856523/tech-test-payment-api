using Pottencial.Ioc;
using Pottencial.Shareable.Config;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(Program));

DependencyInjection.RegisterServices(builder.Services, builder.Configuration);
SwaggerConfig.SetSettingsSwagger(builder.Services, builder.Configuration);

var app = builder.Build();

SwaggerConfig.SetVersionAPISwagger(app);

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();

app.Run();
