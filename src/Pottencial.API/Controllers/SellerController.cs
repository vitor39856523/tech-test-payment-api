﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Shareable.Request;
using Pottencial.Shareable.Response;

namespace Pottencial.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SellerController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly INotificator _notificator;

        public SellerController(IMediator mediator
            , INotificator notificador) : base(notificador)
        {
            _mediator = mediator;
            _notificator = notificador;
        }

        [HttpGet]
        public async Task<IActionResult> GetSeller()
        {
            var (isSuccess, response, exception) = await _mediator.Send(new GetSellerRequest());

            if (_notificator.ExistNotification())
                return BadRequest(_notificator.GetNotifications());

            if (exception != null)
                return BadRequest(exception);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSeller([FromBody] CreateSellerRequest sellerRequest)
        {
            var (isSuccess, response, exception) = await _mediator.Send(sellerRequest);

            if (_notificator.ExistNotification())
                return BadRequest(_notificator.GetNotifications());

            if (exception != null)
                return BadRequest(exception);

            return Ok(sellerRequest);
        }


    }
}
