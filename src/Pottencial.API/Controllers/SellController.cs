﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Shareable.Request;

namespace Pottencial.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SellController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly INotificator _notificator;

        public SellController(IMediator mediator
            , INotificator notificador) : base(notificador)
        {
            _mediator = mediator;
            _notificator = notificador;
        }

        [HttpGet("{idSell:long}")]
        public async Task<IActionResult> GetSell(long idSell)
        {
            var (isSuccess, response, exception) = await _mediator.Send(new GetSellRequest { IdSell = idSell });

            if (_notificator.ExistNotification())
                return BadRequest(_notificator.GetNotifications());

            if (exception != null)
                return BadRequest(exception);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSell([FromBody] CreateSellRequest sellRequest)
        {
            var (isSuccess, response, exception) = await _mediator.Send(sellRequest);

            if (_notificator.ExistNotification())
                return BadRequest(_notificator.GetNotifications());

            if (exception != null)
                return BadRequest(exception);

            return Ok(response);
        }


        [HttpPut("{idSell:long}")]
        public async Task<IActionResult> PutSell(long idSell, PutSellRequest request)
        {
            var (isSuccess, response, exception) = await _mediator.Send(new PutSellRequest { IdSell = idSell, Status = request.Status });

            if (_notificator.ExistNotification())
                return BadRequest(_notificator.GetNotifications());

            if (exception != null)
                return BadRequest(exception);

            return Ok(response);
        }

    }
}
