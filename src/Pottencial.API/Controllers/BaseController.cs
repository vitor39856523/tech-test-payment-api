﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Pottencial.Domain.Interfaces.Services;
using Pottencial.Domain.Notifications;
using System.Text.Json;

namespace Pottencial.API.Controllers
{
    public class BaseController : ControllerBase
    {
        private readonly INotificator _notificator;

        protected BaseController(INotificator notificator)
        {
            _notificator = notificator;
        }

        protected bool ValidOperation()
        {
            return !_notificator.ExistNotification();
        }

        protected dynamic GetNotifications() => _notificator.GetNotifications().Select(n => n.Message);

        protected void NotifyError(string msg)
        {
            _notificator.Handle(new Notification(msg));
        }
    }
}
